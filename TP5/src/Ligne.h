#ifndef LIGNE_H
#define LIGNE_H

#include <iostream>

#include "FigureGeometrique.h"
#include "Point.h"
#include "Couleur.h"

class Ligne : public FigureGeometrique{

private:
    Point _p0, _p1;

public:
    Ligne(const Couleur & couleur, const Point & p0, const Point & p1);
    const Point & getP0() const;
    const Point & getP1() const;
    void afficher() const override;
};

#endif // LIGNE_H
