#include "ViewerFigures.h"
#include "ZoneDessin.h"

#include <gtkmm.h>

int main(int argc, char ** argv) {
    ViewerFigures fenetre(argc,argv);
    ZoneDessin dessin();
	fenetre.addZoneDessin(dessin);
    fenetre.run();
}
