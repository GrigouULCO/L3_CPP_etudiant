#include "ZoneDessin.h"

#include "Ligne.h"
#include "FigureGeometrique.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygoneRegulier.h"
#include "ViewerFigures.h"

ZoneDessin::ZoneDessin(){
	
	Couleur c;
	c._r = 1;
	c._g = 0;
	c._b = 0;
	
	Point a;
	a._x = 0;
	a._y = 0;
	
	Point b;
	b._x = 100;
	b._y = 200;
		
	int rayon = 50, nbPoints = 5;

	_figures.push_back(new Ligne(c, a, b));
	_figures.push_back(new PolygoneRegulier(c,b,rayon,nbPoints));
}
	
ZoneDessin::~ZoneDessin(){
	for(int i = 0; i <= _figures.size();i++){
		delete[] _figures[i];
	}
}
	
bool ZoneDessin::on_expose_event(GdkEventExpose* event){return 0}

bool ZoneDessin::gererClic(GdkEventButton* event){return 0}
