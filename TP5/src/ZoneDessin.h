#ifndef ZONEDESSIN_H
#define ZONEDESSIN_H

#include <iostream>
#include <gtkmm.h>

#include "Ligne.h"
#include "FigureGeometrique.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygoneRegulier.h"
#include "ViewerFigures.h"

class ZoneDessin : public Gtk::DrawingArea{
	
private: 
	std::vector<FigureGeometrique*> _figures;
	
public:
	ZoneDessin();
	~ZoneDessin();
	bool on_expose_event(GdkEventExpose* event);
	bool gererClic(GdkEventButton* event);
};

#endif // ZONEDESSIN_H
