#ifndef VIEWERFIGURES_H
#define VIEWERFIGURES_H

#include <iostream>
#include <gtkmm.h>

#include "ZoneDessin.h"

class ViewerFigures{
	
private:
    Gtk::Main _kit;
	Gtk::Window _window; 
	
public:
	ViewerFigures(int argc, char ** argv): _kit(argc, argv){};
	
	void addZoneDessin(ZoneDessin draw){
		_window.add(draw);
	}
	
	void run(){
		_window.set_title("ViewerFigures");
		_window.set_default_size(640,480);
		_window.show_all();
		_kit.run(_window); 
	}
};

#endif // VIEWERFIGURES_H
