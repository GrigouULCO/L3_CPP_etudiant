#include <iostream>
#include <math.h>

#include "Ligne.h"
#include "FigureGeometrique.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygoneRegulier.h"

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes): FigureGeometrique(couleur){
	
	_nbPoints = nbCotes;
	_points = new Point[nbCotes];
	
	for(int i=0; i<nbCotes; i++){
		_points[i]._x = rayon*cos(i*(M_PI/nbCotes))+centre._x;
		_points[i]._y = rayon*sin(i*(M_PI/nbCotes))+centre._y;
	}
}
    
void PolygoneRegulier::afficher() const{
	std::cout << " Polygone : Couleur(" << getCouleur()._r << "," << getCouleur()._g << "," << getCouleur()._b << "), ";
	for(int i = 0; i < _nbPoints; i++){
		std::cout << "Point[" << i <<"](" << getPoints(i)._x << "," << getPoints(i)._y << "),";
	}
	std::cout << std::endl;
}
    
    
int PolygoneRegulier::getNbPoints() const{
	return _nbPoints;
}
    
const Point & PolygoneRegulier::getPoints(int indice) const{
	return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
	delete[] _points;
}

