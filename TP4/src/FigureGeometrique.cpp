#include "FigureGeometrique.h"

FigureGeometrique::FigureGeometrique(const Couleur & couleur) : _couleur(couleur){ }

const Couleur & FigureGeometrique::getCouleur() const{
    return _couleur;
}

void FigureGeometrique::afficher() const{}
