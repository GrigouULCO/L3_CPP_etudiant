#include <iostream>

#include "Ligne.h"
#include "FigureGeometrique.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygoneRegulier.h"

int main(){
	
	Couleur c;
	c._r = 1;
	c._g = 0;
	c._b = 0;
	
	Point a;
	a._x = 0;
	a._y = 0;
	
	Point b;
	b._x = 100;
	b._y = 200;
	
	int rayon = 50;
	int nbPoints = 5;
	
	Ligne ligne(c ,a ,b);
	PolygoneRegulier polyR(c,b,rayon,nbPoints);
	
	ligne.afficher();
	polyR.afficher();
	
	return 0;
}
