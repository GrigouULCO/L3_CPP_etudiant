#include <iostream>

#include "Ligne.h"
#include "FigureGeometrique.h"
#include "Point.h"
#include "Couleur.h"

Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1) : FigureGeometrique(couleur){
    _p0 = p0;
    _p1 = p1;
}

const Point & Ligne::getP0() const{
	return _p0;
}

const Point & Ligne::getP1() const{
	return _p1;
}

void Ligne::afficher() const{
	std::cout << " Ligne : Couleur(" << getCouleur()._r << "," << getCouleur()._g << "," << getCouleur()._b << "), P0(" 
	<< getP0()._x << "," << getP0()._y << "), P1(" << getP1()._x << "," << getP1()._y << ")" << std::endl;
}
