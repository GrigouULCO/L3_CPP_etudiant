#ifndef FIGUREGEOMETRIQUE_H
#define FIGUREGEOMETRIQUE_H

#include <iostream>

#include "Point.h"
#include "Couleur.h"


class FigureGeometrique{

private:

public:
    Couleur _couleur;

    FigureGeometrique(const Couleur & couleur);
    const Couleur & getCouleur() const;
    virtual void afficher() const;

};

#endif // FIGUREGEOMETRIQUE_H
