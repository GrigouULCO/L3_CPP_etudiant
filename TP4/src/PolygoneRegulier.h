#ifndef POLYR_H
#define POLYR_H

#include <iostream>

#include "Ligne.h"
#include "FigureGeometrique.h"
#include "Point.h"
#include "Couleur.h"
#include "PolygoneRegulier.h"

class PolygoneRegulier : public FigureGeometrique{

private:
    Point* _points;
	int _nbPoints; 
public:
    PolygoneRegulier(const Couleur & couleur, const Point & centre ,int rayon, int nbCotes);
    int getNbPoints() const;
    const Point & getPoints(int indice) const;
    void afficher() const override;
    ~PolygoneRegulier();
};

#endif // POLYR_H
