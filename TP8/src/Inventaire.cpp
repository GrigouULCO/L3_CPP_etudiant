#include "Inventaire.hpp"
std::istream& operator >>(std::istream& is, Inventaire& inv) {
	std::locale vieuxloc = std::locale::global(std::locale("fr_FR.UTF-8"));
	std::string buffer;
	Bouteille b;
	while(is >> b){
		inv._bouteilles.push_back(b);
	}
	std::locale::global(vieuxloc);
	return is;
}

std::ostream& operator <<(std::ostream& os, const Inventaire& inv) {
	for(int i = 0;i < inv._bouteilles.size(); i++){
		os << inv._bouteilles[i];
	}
	return os;
}


