#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <sstream>
Controleur::Controleur(int argc, char ** argv) {
	_vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
	chargerInventaire("test");
	for (auto & v : _vues)
      v->actualiser();
}
std::string Controleur::getTexte(){
	std::string texte;
	std::ostringstream oss117;
	oss117 << _inventaire;
	texte = oss117.str();
	return texte;
}
void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

void Controleur::chargerInventaire(std::string b){
		std::ifstream fichier(b, std::ios::in);
        if(fichier)
        {
                /*string contenu;
                while(getline(fichier, contenu))
                {
					_inventaire << contenu << \n
				}*/
				Bouteille bou;
			while(fichier >> bou){
				_inventaire._bouteilles.push_back(bou);
			}
				
                fichier.close();
        }
        else
        {
			std::cerr << "Impossible d'ouvrir le fichier !" << std::endl;
		}	
	//_inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});
	for (auto & v : _vues)
      v->actualiser();
}
