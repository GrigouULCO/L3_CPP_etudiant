#include "Doubler.hpp"

#include <iostream>

int main() {
	int a = 42;
	int *p = &a;
	*p = 37;
	
	int *t = new int [10];
	t[3] = 42;
	delete t[];
	t = nullptr;
	
	std::cout << a << std::endl;
	
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;
    return 0;
}

