#ifndef LISTE_H_
#define LISTE_H_

struct Noeud{
    int _valeur;
    Noeud *_suivant;
};

struct liste{
    Noeud *_tete;
    
    void ajouterDevant(int valeur);
    int getTaille();
    int getElement(int indice);
};

#endif
