#include "vecteur3.h"
#include <math.h>
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Vecteur3) { };

TEST(Vecteur3, test_vecteur3_1) {
    Vecteur3 v{1,2,3};
    double resultat = sqrt(pow(1,2)+pow(2,2)+pow(3,2));
    CHECK_EQUAL(resultat, v.norme());
}

TEST(Vecteur3, test_vecteur3_2) {
    Vecteur3 a{1,2,3};
    Vecteur3 b{1,2,3};
    double resultat = (1*1)+(2*2)+(3*3);
    CHECK_EQUAL(resultat, produitScalaire(a,b));
}

TEST(Vecteur3, test_vecteur3_3) {
    Vecteur3 a{1,2,3};
    Vecteur3 b{1,2,3};

    Vecteur3 c{2,4,6};
    Vecteur3 d = addition(a,b);

    CHECK_EQUAL(c.x,d.x);
    CHECK_EQUAL(c.y,d.y);
    CHECK_EQUAL(c.z,d.z);
}
