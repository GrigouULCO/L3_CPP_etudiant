#include "fibonacci.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Fibonacci) { };

TEST(Fibonacci, test_fibo_1) { // premier test unitaire
    int listresult[] = {0,1,1,2,3};
    for(int i=0; i<5; i++){
		CHECK_EQUAL(listresult[i], fibonacciRecursif(i));
    }
}

TEST(Fibonacci, test_fibo_2) { // deuxième test unitaire
    int listresult[] = {0,1,1,2,3};
    for(int i=0; i<5; i++){
		CHECK_EQUAL(listresult[i], fibonacciIteratif(i));
    }
}
