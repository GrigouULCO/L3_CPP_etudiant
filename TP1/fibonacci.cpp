#include <iostream>
#include "fibonacci.h"

using namespace std;

int fibonacciRecursif(int x)
{
	if (x < 2) {
        return x;
    } else {
        return fibonacciRecursif(x-1)+fibonacciRecursif(x-2);
    }
}

int fibonacciIteratif(int n)
{
  if (n==0){
	  return 0;
  }else{
	int u = 0;
	int v = 1;
	int i, t;

	for(i=0; i<n; i++) {
		t = u + v;
		u = v;
		v = t;
	}
	return u;
  }
}
