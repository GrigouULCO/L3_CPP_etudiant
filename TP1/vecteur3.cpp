#include <iostream>
#include <math.h>
#include "vecteur3.h"

using namespace std;

void Vecteur3::afficher() {
    cout << "Vecteur : (" << x << ", " << y << ", "  << z <<")" << endl;
}

void afficher(const Vecteur3 & v) {
    cout << "Vecteur : (" << v.x << ", " << v.y << ", "  << v.z <<")" << endl;
}

double Vecteur3::norme(){
	double resultat = sqrt(pow(x,2)+pow(y,2)+pow(z,2));
    cout << "Norme : " << resultat << endl;
    return resultat;
}

double produitScalaire(Vecteur3 a, Vecteur3 b){
    double resultat = (a.x*b.x)+(a.y*b.y)+(a.z*b.z);
    cout << "Produit Scalaire : " << resultat << endl;
    return resultat;
}

Vecteur3 addition(Vecteur3 a, Vecteur3 b){
    Vecteur3 c;
    c.x = (a.x+b.x);
    c.y = (a.y+b.y);
    c.z = (a.z+b.z);
    return c;
}
