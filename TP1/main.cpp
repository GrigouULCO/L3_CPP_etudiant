#include <iostream>
#include "fibonacci.h"
#include "vecteur3.h"

using namespace std;

int main()
{
    //cout << fibonacciRecursif(7) << endl;
    //cout << fibonacciIteratif(7) << endl;
    Vecteur3 a{2,3,6};
    Vecteur3 b{1,4,7};
    a.afficher();
    afficher(a);
    a.norme();
    produitScalaire(a,b);
    Vecteur3 c = addition(a,b);
    c.afficher();
    return 0;
}
