#ifndef FIBONACCI_H_
#define FIBONACCI_H_

int fibonacciRecursif(int x);
int fibonacciIteratif(int n);

#endif
