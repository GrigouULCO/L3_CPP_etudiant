#ifndef VECTEUR3_H_
#define VECTEUR3_H_

struct Vecteur3{
    float x,y,z;
    void afficher();
    double norme();
};

void afficher(const Vecteur3 & v);
double produitScalaire(Vecteur3 a, Vecteur3 b);
Vecteur3 addition(Vecteur3 a, Vecteur3 b);
#endif
